#include <iostream>
#include <vector>

void OptimizeIO() {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::cout.tie(nullptr);
}

int main(int argc, char* argv[]) {
  OptimizeIO();

  return 0;
}
